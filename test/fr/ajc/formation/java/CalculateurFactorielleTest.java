package fr.ajc.formation.java;
//importer les resources necÚssaires pour 
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void testFactorielle_de_0_should_be_1() {
		assertEquals(1, CalculateurFactorielle.factorielle(0));
	}
	@Test
	void testFactorielle_de_1_should_be_1() {
		assertEquals(1, CalculateurFactorielle.factorielle(1));
	}
	@Test
	void testFactorielle_de_3_should_be_6() {
		assertEquals(6, CalculateurFactorielle.factorielle(3));
	}
	
	@Test
	void testFactorielle_de_6_should_be_720() {
		assertEquals(721, CalculateurFactorielle.factorielle(6),"factorielle_de_6_should_be_720");
	}
	
		/*void testFactorielle_de_6_should_be_720() {
			assertTrue(CalculateurFactorielle.factorielle(6)==720);
		}*/	
		
		
	}

